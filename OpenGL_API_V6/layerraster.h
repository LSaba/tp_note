#ifndef LAYERRASTER_H
#define LAYERRASTER_H

#include "gdal_priv.h"
#include <vector>
#include <glm/glm.hpp>
#include <iostream>
#include "cpl_conv.h" // for CPLMalloc()
#include <cstdio>


class LayerRaster
{
public:
    LayerRaster(const char* filePath);
    void openGeotiff(); // file loader function
    int* getGeotiffSize();
    float* getPixelSize();
    void fetchGeotiff();
    std::vector<glm::vec2> color;
    std::vector<glm::vec3> dataTIFF; // data that will be put in the vertexBuffer
    std::vector<float> posCam;
    std::vector<glm::vec3> objTraj;
    std::vector<unsigned int> indexTable;; // data that will be put in the indexBuffer
    int widthTIFF;
    int heightTIFF;
    int numberOfBands; // number of raster bands
    const char* getGeotiffProjection();
    void createIndexTable();
    std::vector<std::vector<float>> MNT;
private:
    const char* filePath;
    GDALDataset *geotiffData; // data in the GDALDataset format
};

#endif // LAYERRASTER_H
