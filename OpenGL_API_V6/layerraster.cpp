#include "layerraster.h"


LayerRaster::LayerRaster(const char* filePath)
{
    this->filePath = filePath;

    openGeotiff();

    // initialisation of width/height/bands
    this->widthTIFF = getGeotiffSize()[0];
    this->heightTIFF = getGeotiffSize()[1];
    this->numberOfBands = getGeotiffSize()[2];

    createIndexTable();
    fetchGeotiff();
}


void LayerRaster::openGeotiff()
{
    /* Connexion to GeoTIFF using GDAL */

    GDALAllRegister();
    this->geotiffData = (GDALDataset *) GDALOpen(this->filePath, GA_ReadOnly );
}


void LayerRaster::createIndexTable()
{
    int tableSize = (8 + (this->widthTIFF + this->heightTIFF - 4 + (this->widthTIFF - 1) * (this->heightTIFF - 1)) * 6) * this->numberOfBands;
    std::vector<unsigned int> indexTable;
    // the loop will store each time 6 values of IndexTable for 2 triangles
    for (int i = 0; i < this->heightTIFF - 1; i++) {
        for (int j = 0; j < this->widthTIFF; j++) {
            // first triangle
            // point in line i and column j, 6 means each time the values restart from the 7th value and k if there are different raster bands
            indexTable.push_back( i * this->widthTIFF + j );
            // point in line i+1 and column j
            indexTable.push_back( (i + 1) * this->widthTIFF + j );
            // point in line i+1 and column j+1
            indexTable.push_back( (i + 1) * this->widthTIFF + j + 1);

            // second triangle
            // point in line i and column j
            indexTable.push_back( i * this->widthTIFF + j );
            // point in line i and column j+1
            indexTable.push_back( i * this->widthTIFF + j + 1 );
            // point in line i+1 and column j+1
            indexTable.push_back( (i + 1) * this->widthTIFF + j + 1 );
        }
    }
    this->indexTable = indexTable;
}


void LayerRaster::fetchGeotiff(){
    // result defines a vect of GeoTIFF pixel data
    std::vector<glm::vec3> result;
//    std::vector<glm::vec3> trajectoire;
    std::vector<std::vector<float>> out = std::vector<std::vector<float>>(widthTIFF, std::vector<float>(heightTIFF, 0));
    double adfMinMax[2];
    float xPosCam=0;
    float yPosCam=0;
    float zPosCam=0;
    int rayon = 20;

    for(int k = 1; k <= this->numberOfBands; k++){
        GDALRasterBand  *rasterBand = this->geotiffData->GetRasterBand(k);
        float *pafScanline;
        pafScanline = (float *) CPLMalloc(sizeof(float)*this->widthTIFF*this->heightTIFF);
        // RasterIO function browse the raster bands and store the data in the pafscanline
        if(rasterBand->RasterIO(GF_Read, 0, 0, this->widthTIFF, this->heightTIFF, pafScanline, this->widthTIFF, this->heightTIFF, GDT_Float32, 0, 0) == CE_None)
        {
            // Construct a height map based on the xres and yres for each group of four dots
            for (int i = 0; i < this->heightTIFF; i++)
            {
                for (int j = 0; j < this->widthTIFF; j++)
                {
                    if (pafScanline[i*this->widthTIFF + j] > 0) {
                        out[j][i] = pafScanline[(i)*widthTIFF + j];

                        if (out[j][i] > yPosCam) {
                             xPosCam = (float)i;
                             zPosCam = (float)j;
                             yPosCam = (float)out[i][j];
                    } else
                                       out[j][i] = 0;
                               }
                }
            }

            for (int i = 0; i < this->heightTIFF; i++)
            {
                for (int j = 0; j < this->widthTIFF; j++)
                {
                    out[j][i] = out[j][i]/yPosCam*10;
                }
             }

            posCam.push_back(xPosCam);
            posCam.push_back(yPosCam);
            posCam.push_back(zPosCam);

         } else {
            std::cout<<"Error: can't read raster bands"<<std::endl;
        }

        CPLFree(pafScanline);
    }

    // Time to construct a height map based on the xres and yres for each group of four dots
        for (int i = 0; i < out.size() - 1; i++)
        {
            for (int j = 0; j < out[i].size() - 1; j++)
            {
                result.push_back(glm::vec3( i , out[i][j],j ));
                result.push_back(glm::vec3(  (i + 1) ,out[i+1][j],j   ));
                result.push_back(glm::vec3( i ,out[i][j+1], (j + 1)  ));
                result.push_back(glm::vec3(  (i + 1) ,out[i+1][j+1],(j+1) ));
                result.push_back(glm::vec3( (i + 1) , out[i+1][j],j   ));
                result.push_back(glm::vec3( i ,out[i][j+1] ,(j + 1) ));


                color.push_back(glm::vec2((float)i/(float)out.size() ,(float)(1 + j)/(float)out[i].size() ));
                color.push_back(glm::vec2((float)(i + 1)/(float)out.size() ,(float)(1 + j)/(float)out[i].size() ));
                color.push_back(glm::vec2((float)(i + 1)/(float)out.size() ,(float)j/(float)out[i].size() ));
                color.push_back(glm::vec2((float)i/(float)out.size() ,(float)(j + 1)/(float)out[i].size() ));
                color.push_back(glm::vec2((float)i/(float)out.size() ,(float)(j)/(float)out[i].size() ));
                color.push_back(glm::vec2((float)(i+1)/(float)out.size() ,(float)j/(float)out[i].size() ));

//                if (i == j )
//                    trajectoire.push_back(glm::vec3( i , out[i][j]/yPosCam *10,j ));
            }
        }
    this->MNT = out;
    this->dataTIFF = result;
//    this->objTraj = trajectoire;
}

int* LayerRaster::getGeotiffSize()
{
    if( this->geotiffData == NULL )
    {
        std::cout<< "Fail to open the GeoTIFF" <<std::endl;
        return NULL;
    }

    int* dataArray = new int[3];
    // Dimensions
    dataArray[0] = this->geotiffData->GetRasterXSize();
    dataArray[1] = this->geotiffData->GetRasterYSize();
    // Nb of Bands
    dataArray[2] = this->geotiffData->GetRasterCount();

    return dataArray;
}

