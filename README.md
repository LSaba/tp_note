# TP_note



## Installer le TP

On commence par installer le projet zippé en local, et on l'ouvre avec un IDE tel que QTcreator ou VisualStudio. Ensuite, on installe les librairies nécessaires pour la visualisation de la suite, telles que GDal, Glew, GLM et GLFW en suivant ses commandes sur le terminal:

```
sudo add-apt-repository ppa:ubuntugis/ppa
sudo apt-get update
sudo apt-get install gdal-bin
sudo apt-get install libgdal-dev
export CPLUS_INCLUDE_PATH=/usr/include/gdal
export C_INCLUDE_PATH=/usr/include/gdal
gdalinfo --version
```

Ensuite, on configure le CMakeFile pour lire les fichiers au bon endroit au cas où leur disposition est changée, on rajoute aussi le package de GDal : 
```
find_package( OpenGL REQUIRED )
```


## Fichiers changés 

Je suis partie du TP suivi pendant le cours d'OpenGL pour arriver au résultat.


layerraster.cpp et layerraster.h définissent la classe LayerRaster qui permet d'ouvrir une couche raster, en particulier un MNT. Une grande partie de ce code a été inspiré du code que nous avons réalisé pour le projet du mini-SIG en Back. 

Donc c'est construit en sorte d'ouvrir une couche raster, récupérer son width and height pour pouvoir les parcourir afin de définir le vecteur des données geotiff. (La méthode createIndexTable permet de gagner bcp en mémoire quand on travaille avec les géotiffs, cette méthode n'a pas été utilisée pour cet exercice.) 

## Construction du MNT

Ceci crée donc les vecteurs vertices (normalisés pour pouvoir visualiser les hauteurs) et randomUV qui sont utilisées pour construire l'objet MNT ave une texture de montagne.

Ensuite, on positionne l'objet o, notre cube qui va se déplacer d'une manière aléatoire, le MNT puis la caméra qui va avoir la possibilité de bouger avec les touches de notre clavier et réaliiser un saut en appuyant sur Espace. 

Les positions de la caméra et l'objet sont définites à côté des grandes hauteurs du MNT pour pouvoir vérifier qu'ils ne rentrent pas dans le MNT mais le respectent bien comme si c'était un sol. 




## Trajectoire de l'objet

Plusieurs trajectoires ont été définis au long de ce projet, dans la méthode fetchgeotiff, on retrouve la trajectoire au long de la diagonale. La trajectoire visualisée quand on lance le projet est une trajectoire aléatoire planaire suivant le MNT.

## Trajectoire de la caméra

La caméra n'a pas de trajectoire prédéfinie mais suit les touches de l'utilisateur qui la contrôle avec son clavier et souris. 


## La touche Espace : principe du saut de caméra :

En appuyant sur la touche Espace on remarque que la caméra saute d'un cran, ceci est définit en changeant "navigationcontrol" qui commence si et seulement si le mode_jump est à 0 ensuite, ensuite la position.y de la caméra augmente jusqu'à dépasser la valeur du seuil, le mode_jump est à 1. Une fois, la caméra atteint le haut niveau, elle retombe pour se remettre au niveau du MNT. 

